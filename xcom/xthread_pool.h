#ifndef XTHREAD_POOL_H
#define XTHREAD_POOL_H
#include <vector>

#ifdef _WIN32
#ifdef XCOM_EXPORTS
#define XCOM_API __declspec(dllexport)
#else
#define XCOM_API __declspec(dllimport)
#endif
#else
#define XCOM_API
#endif

class XThread;
class XTask;
class XCOM_API XThreadPool
{
public:
    // 单件模式
    static XThreadPool* Get()
    {
        static XThreadPool p;
        return &p;
    }
    // 初始化所有线程并启动线程
    void Init(int thread_count);

    // 分发线程
    void Dispatch(XTask* task);

    XThreadPool() {};
private:
    // 线程数量
    int thread_count_ = 0;
    int last_thread_ = -1;
    // 线程池线程
    std::vector<XThread *> threads_;
    
};
#endif

