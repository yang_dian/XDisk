#include <iostream>
#include <thread>
#include <chrono>
#include "xthread_pool.h"
#include "xserver_task.h"
#include "xfile_server_task.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <signal.h>
#endif

using namespace std;
using namespace std::chrono;

static void ListenCB(int sock, struct sockaddr* addr, int socklen, void* arg)
{
    cout << "ListenCB in main" << endl;
    auto* task = new XFileServerTask();
    task->set_sock(sock);
    XThreadPool::Get()->Dispatch(task);
}

int main(int argc, char* argv[])
{
#ifdef _WIN32
    // 初始化socket库
    WSADATA wver;
    WSAStartup(MAKEWORD(2, 2), &wver);
#else
    // 使用断开连接socket，会发出此信号，造成程序退出
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        return 1;
#endif
    // 获取端口和线程池线程数量
    int server_port = 21002;
    int thread_count = 10;
    if (argc > 1)
    {
        server_port = atoi(argv[1]);
    }
    if (argc > 2)
    {
        thread_count = atoi(argv[2]);
    }
    if (argc == 1)
        cout << "xdisk_server port thread_count" << endl;

    // 初始化主线程池
    XThreadPool::Get()->Init(thread_count);

    XThreadPool server_pool;
    server_pool.Init(1);
    auto* task = new XServerTask();
    task->set_server_port(server_port);
    task->ListenCB = ListenCB;
    server_pool.Dispatch(task);

    for (;;)
    {
        this_thread::sleep_for(milliseconds(1));
    }

    return 0;
}