#ifndef XSERVICE_CLIENT_H
#define XSERVICE_CLIENT_H

#include "xmsg_event.h"
#include "xthread_pool.h"
class XCOM_API XServiceClient : public XMsgEvent
{
public:
    XServiceClient();
    virtual ~XServiceClient();
    // 将任务加入到线程池中，进行连接
    virtual void StartConnect();

    // 
private:
    XThreadPool* thread_pool_ = 0;
};

#endif

