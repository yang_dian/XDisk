#include <iostream>
#include <string.h>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include "xcom_task.h"
#include "xtools.h"

using namespace std;

static void SReadCB(struct bufferevent* bev, void* ctx)
{
    auto* task = (XComTask*)ctx;
    task->ReadCB();
}

static void SWriteCB(struct bufferevent* bev, void* ctx)
{
    auto* task = (XComTask*)ctx;
    task->WriteCB();
}

static void SEventCB(struct bufferevent* bev, short what, void* ctx)
{
    auto* task = (XComTask*)ctx;
    task->EventCB(what);
}

XComTask::XComTask()
{
    mux_ = new mutex;
}

XComTask::~XComTask()
{
    delete mux_;
}

bool XComTask::Connect()
{

    sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(server_port_);
    evutil_inet_pton(AF_INET, server_ip_, &sin.sin_addr.s_addr);
    lock_guard<mutex> lock(*mux_);
    is_connected_ = false;
    is_connecting_ = false;
    if (!bev_)
    {
        InitBev(-1);
    }
    if (!bev_)
    {
        LOGERROR("XComTask::Connect failed! bev is null!");
        return false;
    }
    int re = bufferevent_socket_connect(bev_, (sockaddr*)&sin, sizeof(sin));
    if (re != 0)
    {
        return false;
    }
    // 开始连接
    is_connecting_ = true;
    return true;
}

bool XComTask::Init()
{
    // 区分服务端还是客户端
    int comsock = this->sock();
    if (comsock <= 0)
    {
        comsock = -1;
    }
    {
        lock_guard<mutex> lock(*mux_);
        InitBev(comsock);
    }
    
    //timeval tv = { 10, 0 };
    //bufferevent_set_timeouts(bev_, &tv, &tv);

    // 连接服务器
    if (server_ip_[0] == '\0')
    {
        return true;
    }
    return Connect();
}

void XComTask::Close()
{
    lock_guard<mutex> lock(*mux_);
    is_connected_ = false;
    is_connecting_ = false;
    if (bev_)
    {
        bufferevent_free(bev_);
    }
    bev_ = NULL;
    if (msg_.data_)
        delete msg_.data_;
    memset(&msg_, 0, sizeof(msg_));
}

int XComTask::Read(void* data, int datasize)
{
    if (!bev_)
    {
        LOGERROR("bev not set");
        return 0;
    }
    int re = bufferevent_read(bev_, data, datasize);
    return re;
}

void XComTask::set_server_ip(const char* ip)
{
    strncpy(this->server_ip_, ip, sizeof(server_ip_));
}

void XComTask::EventCB(short what)
{
    cout << "SEventCB:" << what << endl;
    if (what & BEV_EVENT_CONNECTED)
    {
        stringstream ss;
        ss << "BEV_EVENT_CONNECTED connect server " << server_ip_ << ":" << server_port_ << " success!";
        LOGINFO(ss.str().c_str());
        // bufferevent_write(bev_, "OK", 3);
        // 通知连接成功
        is_connected_ = true;
        is_connecting_ = false;
        ConnectedCB();
    }

    // 退出要处理缓冲内容
    if (what & BEV_EVENT_ERROR || what & BEV_EVENT_TIMEOUT)
    {
        cout << "BEV_EVENT_ERROR or BEV_EVENT_TIMEOUT" << endl;
        Close();
    }
    if (what & BEV_EVENT_EOF)
    {
        cout << "BEV_EVENT_EOF" << endl;
        Close();
    }
}

//bool XComTask::Write(const XMsg* msg)
//{
//    if (!bev_ || !msg || !msg->data_ || msg->size_ <= 0)
//    {
//        return false;
//    }
//    // 1 写入消息头
//    int re = bufferevent_write(bev_, (char*)msg, sizeof(XMsghead));
//    if (re != 0)
//    {
//        return false;
//    }
//    re = bufferevent_write(bev_, msg->data, msg->size);
//    if (re != 0)
//    {
//        return false;
//    }
//    // cout << "XMsghead size:" << sizeof(XMsghead) << " msg size:" << msg->size << endl;
//    // 2 写入消息内容
//    return true;
//}

bool XComTask::Write(const void* data, int size)
{
    lock_guard<mutex> lock(*mux_);
    if (!bev_ || !data || size <= 0)
    {
        return false;
    }
    int re = bufferevent_write(bev_, (char*)data, size);
    if (re != 0)
    {
        return false;
    }
    return true;
}

void XComTask::BeginWrite()
{
    if (!bev_)
    {
        return;
    }
    bufferevent_trigger(bev_, EV_WRITE, 0);
}

bool XComTask::InitBev(int sock)
{
    // 用bufferevent 建立连接 
    // 1. 创建bufferevent上下文  -1自动创建socket
    bev_ = bufferevent_socket_new(base(), sock, BEV_OPT_CLOSE_ON_FREE);
    if (!bev_)
    {
        LOGERROR("bufferevent_socket_new failed!");
        return false;
    }
    // 设置回调函数
    bufferevent_setcb(bev_, SReadCB, SWriteCB, SEventCB, this);
    bufferevent_enable(bev_, EV_READ | EV_WRITE);
    return true;
}
