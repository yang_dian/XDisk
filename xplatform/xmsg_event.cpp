#include <iostream>
#include <sstream>
#include <vector>
#include "xmsg_event.h"
#include "xmsg_com.pb.h"
#include "xtools.h"

using namespace std;
using namespace xmsg;
using namespace google;
using namespace protobuf;

// 同一个类型只能有一个回调函数
static vector<XMsgEvent::MsgCBFunc> msg_callback(MSG_MAX_TYPE);

void XMsgEvent::RegCB(MsgType type, MsgCBFunc func)
{
    if (msg_callback[type])
    {
        stringstream ss;
        ss << "RegCB is error," << type << "have been set ";
        LOGERROR(ss.str().c_str());
        return;
    }  
    msg_callback[type] = func;
}

void XMsgEvent::ReadCB()
{
    if (!RecvMsg())
    {
        Clear();
        return;
    }
    auto* msg = GetMsg();
    if (!msg) return;
    LOGDEBUG(pb_head_->DebugString().c_str());
    ReadCB(pb_head_, msg);
    Clear();
}

void XMsgEvent::ReadCB(xmsg::XMsgHead* head, XMsg* msg)
{
    // 回调消息函数
    auto func = msg_callback[head->msg_type()];
    if (!func)
    {
        LOGDEBUG("msg error func not set!");
        return;
    }
    (this->*func)(head, msg);
}

bool XMsgEvent::RecvMsg()
{
    // 解包
    // 1 消息头大小
    if (!head_.size_)
    {
        // 消息头大小
        int len = Read(&head_.size_, sizeof(head_.size_));
        if (len <= 0 || head_.size_ <= 0)
        {
            return false;
        }
        // 分配消息头空间 读取消息头（鉴权，消息大小）
        if (!head_.Alloc(head_.size_))
        {
            cerr << "head_.Alloc failed!" << endl;
            return false;
        }
    }
    
    // 2 开始接收消息头 （鉴权，消息大小）
    if (!head_.Recved())
    {
        int len = Read(head_.data_ + head_.recv_size_, head_.size_ - head_.recv_size_);
        if (len <= 0)
        {
            return true;
        }
        head_.recv_size_ += len;
        if (!head_.Recved())
        {
            return true;
        }
        // 完整的头部数据接收完成
        // 反序列化
        if (!pb_head_)
        {
            pb_head_ = new XMsgHead();
        }
        if (!pb_head_->ParseFromArray(head_.data_, head_.size_))
        {
            cerr << "pb_head.ParseFromArray failed!" << endl;
            return false;
        }
        // 鉴权
        // 消息内容大小
        // 分配消息内容空间
        if (!msg_.Alloc(pb_head_->msg_size()))
        {
            cerr << "msg_.Alloc failed!" << endl;
            return false;
        }
        // 消息类型
        msg_.type_ = pb_head_->msg_type();
    }

    // 3 开始接收消息内容
    if (!msg_.Recved())
    {
        int len = Read(msg_.data_ + msg_.recv_size_, msg_.size_ - msg_.recv_size_);
        if (len <= 0)
        {
            return true;
        }
        msg_.recv_size_ += len;
    }
    if (msg_.Recved())
    {
        cout << "msg_.Recved()" << endl;
    }

    return true;
}

XMsg* XMsgEvent::GetMsg()
{
    if (msg_.Recved())
        return &msg_;
    return nullptr;
}

bool XMsgEvent::SendMsg(xmsg::MsgType type, const Message* message)
{
    if (!message)
    {
        return false;
    }
    XMsgHead head;
    head.set_msg_type(type);
    return SendMsg(&head, message);
}

bool XMsgEvent::SendMsg(XMsgHead* head, const Message* message)
{
    if (!head || !message)
    {
        return false;
    }
    // 封包
    // 消息内容序列化
    string msg_str = message->SerializeAsString();
    int msg_size = msg_str.size();
    XMsg msg;
    msg.data_ = (char*)msg_str.data();
    msg.size_ = msg_size;
    return SendMsg(head, &msg);
}

bool XMsgEvent::SendMsg(XMsgHead* head, XMsg* msg)
{
    if (!head || !msg)
    {
        return false;
    }
    head->set_msg_size(msg->size_);
    // 消息头序列化
    string head_str;
    LOGDEBUG(head->DebugString().c_str());
    head->SerializeToString(&head_str);
    int headsize = head_str.size();

    // 1 发送消息头大小 4字节 暂时不考虑字节序问题
    int re = Write(&headsize, sizeof(headsize));
    if (!re)
        return false;
    // 2 发送消息头（pb序列化）XMsgHead (设置消息内容的大小）
    re = Write(head_str.data(), headsize);
    if (!re)
        return false;
    // 3 发送消息内容 （pb序列化）业务proto
    re = Write(msg->data_, msg->size_);
    if (!re)
        return false;

    return true;
}

void XMsgEvent::Clear()
{
    head_.Clear();
    msg_.Clear();
}

void XMsgEvent::Close()
{
    Clear();
    XComTask::Close();
}


