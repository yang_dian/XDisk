#include <sstream>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include "xservice.h"
#include "xtools.h"
#include "xservice_handle.h"
using namespace std;

static void SListenCB(struct evconnlistener* ev, evutil_socket_t sock, struct sockaddr* client_addr, int socklen, void* arg)
{
    LOGDEBUG("SListenCB");
    auto* task = (XService*)arg;
    task->ListenCB(sock, client_addr, socklen);
}

XService::XService()
{
    this->thread_client_pool_ = XThreadPoolFactory::Create();
    this->thread_listen_pool_ = XThreadPoolFactory::Create();
}

XService::~XService()
{
    delete thread_client_pool_;
    thread_client_pool_ = NULL; 
    delete thread_listen_pool_;
    thread_listen_pool_ = NULL;
}

bool XService::Init()
{
    if (server_port_ <= 0)
    {
        LOGERROR("server_port_ not set!");
        return false;
    }
    // 绑定端口
    sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(server_port_);
    auto* evc = evconnlistener_new_bind(base(), SListenCB, this, LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE, 10, (sockaddr*)&sin, sizeof(sin));
    if (!evc)
    {
        stringstream ss;
        ss << "listen port " << server_port_ << " failed!" << endl;
        LOGERROR(ss.str().c_str());
        return false;
    }
    stringstream ss;
    ss << "listen port " << server_port_ << " success!" << endl;
    LOGINFO(ss.str().c_str());
    return true;
}

bool XService::Start()
{
    thread_listen_pool_->Init(1);
    thread_client_pool_->Init(thread_count_);
    thread_client_pool_->Dispatch(this);

    return true;
}

void XService::ListenCB(int client_socket, sockaddr* client_addr, int socklen)
{
    // 创建客户端处理对象
    auto* handle = CreateServiceHandle();
    handle->set_sock(client_socket);
    stringstream ss;
    char ip[16] = { 0 };
    auto* addr = (sockaddr_in *)client_addr;
    evutil_inet_ntop(AF_INET, &addr->sin_addr.s_addr, ip, sizeof(ip));
    ss << "client ip:" << ip << " port:" << addr->sin_port << endl;
    LOGINFO(ss.str().c_str());

    // 任务加入到线程池
    handle->set_client_ip(ip);
    handle->set_client_port(addr->sin_port);
    thread_client_pool_->Dispatch(handle);
}
