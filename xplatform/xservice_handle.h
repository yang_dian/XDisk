#ifndef XSERVICE_HANDLE_H
#define XSERVICE_HANDLE_H
#include "xmsg_event.h"

class XCOM_API XServiceHandle : public XMsgEvent
{
public:
    void set_client_ip(const char* ip);
    const char* client_ip() { return client_ip_; }

    void set_client_port(int port) { client_port_ = port; }
    int get_client_port() { return client_port_; }
private:
    char client_ip_[16] = { 0 };
    int client_port_ = 0;
};

#endif

