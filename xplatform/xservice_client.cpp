#include "xservice_client.h"

XServiceClient::XServiceClient()
{
    thread_pool_ = XThreadPoolFactory::Create();
}

XServiceClient::~XServiceClient()
{
    delete thread_pool_;
    thread_pool_ = NULL;
}

void XServiceClient::StartConnect()
{
    thread_pool_->Init(1);
    thread_pool_->Dispatch(this);
}
