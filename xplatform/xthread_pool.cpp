#include <iostream>
#include <thread>
#include "xthread_pool.h"
#include "xthread.h"
#include "xtask.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <signal.h>
#endif

using namespace std;
using namespace std::chrono;

// 用于线程的循环退出判断
static bool is_exit_all = false;

// 所有的线程对象
static std::vector<XThread*> all_threads;
static mutex all_threads_mutex;

void XThreadPool::ExitAllThread()
{
    is_exit_all = true;
    {
        lock_guard<mutex> lock(all_threads_mutex);
        for (auto&& t : all_threads)
        {
            t->Exit();
        }
    }
    this_thread::sleep_for(milliseconds(200));
}

void XThreadPool::Wait()
{
    while (!is_exit_all)
    {
        this_thread::sleep_for(milliseconds(100));
    }
}

class CXThreadPool : public XThreadPool
{
public:
    void Init(int thread_count)
    {
        this->thread_count_ = thread_count;
        this->last_thread_ = -1;
        for (int i = 0; i < thread_count; i++)
        {
            XThread* t = new XThread();
            t->id = i + 1;
            cout << "Create thread " << i << endl;
            // 启动线程
            t->Start();
            threads_.push_back(t);
            {
                lock_guard<mutex> lock(all_threads_mutex);
                all_threads.push_back(t);
            }
            this_thread::sleep_for(milliseconds(10));
        }
    }

    void Dispatch(XTask* task)
    {
        // 轮询
        if (!task)
            return;
        int tid = (last_thread_ + 1) % thread_count_;
        last_thread_ = tid;
        XThread* t = threads_[tid];

        t->AddTask(task);
        // 激活线程
        t->Activate();
    }

private:
    // 线程数量
    int thread_count_ = 0;
    int last_thread_ = -1;
    // 线程池线程
    std::vector<XThread*> threads_;
};

XThreadPool* XThreadPoolFactory::Create()
{
    // socket库初始化
    static mutex mux;
    static bool is_init = false;
    {
        lock_guard<mutex> lock(mux);
        if (!is_init)
        {
#ifdef _WIN32
            // 初始化socket库
            WSADATA wver;
            WSAStartup(MAKEWORD(2, 2), &wver);
#else
            // 使用断开连接socket，会发出此信号，造成程序退出
            if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
                return 1;
#endif
            is_init = true;
        }
    }
    
    return new CXThreadPool();;
}

