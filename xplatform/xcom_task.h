#ifndef XCOM_TASK_H
#define XCOM_TASK_H
#include "xtask.h"
#include "xmsg.h"
class XCOM_API XComTask :
    public XTask
{
public:
    XComTask();
    virtual ~XComTask();

    // 开始连接服务器，调用成员 server_ip_, server_port_
    // 考虑自动重连
    virtual bool Connect();

    virtual bool Init();
    virtual void Close();

    int Read(void* data, int datasize);

    void set_server_ip(const char* ip);
    void set_port(int port) { server_port_ = port; }

    void EventCB(short what);

    // 发送消息
    // virtual bool Write(const XMsg* msg);
    virtual bool Write(const void* data, int size);
    // 连接成功的消息回调，由业务类重载
    virtual void ConnectedCB() {};

    // 当关闭消息接收时，数据将发送到此函数，由业务模块重载
    virtual void ReadCB(void* data, int size) {};

    // 接收到消息的回调，由业务类重载 返回true正常，返回false退出当前的消息处理,不处理下一条消息
    // virtual bool ReadCB(const XMsg *msg) = 0;
    virtual void ReadCB() = 0;

    // 写入数据的回调
    virtual void WriteCB() {}

    // 激活写入回调
    virtual void BeginWrite();

    void set_is_recv_msg(bool is_recv_msg) {is_recv_msg_ = is_recv_msg;}

    bool is_connecting() { return is_connecting_; }
    bool is_connected() { return is_connected_; }

protected:
    // 读取缓存
    char read_buf_[4096] = { 0 };

private:
    bool InitBev(int sock);

    // 服务器IP
    char server_ip_[16] = {0};

    // 服务器端口
    int server_port_ = 0;

    struct bufferevent* bev_ = 0;

    // 数据包缓存
    XMsg msg_;

    // 是否接收消息，接收的消息会调用void ReadCB(const XMsg *msg);
    // 不接收消息调用 void ReadCB(void *data, int size);
    bool is_recv_msg_ = true;

    // 客户端的连接状态 
    // 1 未处理 => 开始连接 （加入到线程池处理）
    // 2 连接中 =>等待连接成功
    // 3 已连接 => 做业务操作
    // 4 连接后失败 => 根据连接间隔时间，开始连接
    bool is_connecting_ = true;
    bool is_connected_ = false;
    std::mutex *mux_ = 0;
};

#endif

