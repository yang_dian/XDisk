#pragma once
#include "xmsg_type.pb.h"
#include "xmsg.h"
#include "xcom_task.h"
#include "xmsg_com.pb.h"

// 不调用bufferevent接口，直接调用XComTask封装
class XCOM_API XMsgEvent : public XComTask
{
public:
    // 接收消息，分发消息
    virtual void ReadCB();
    // 消息回调函数，默认发送到用户注册的函数，路由重载
    virtual void ReadCB(xmsg::XMsgHead* head, XMsg* msg);

    // 接收数据包，1 正确接收到消息 （调用消息处理函数）  2 消息接收不完整，（等待下一次接收） 3 消息接收出错（退出清理空间）
    // return: 1 2 返回true  3 返回false
    bool RecvMsg();
    class XMsg* GetMsg();

    // 发送消息，包含头部（自动创建）
    // return 发送错误
    virtual bool SendMsg(xmsg::MsgType type, const google::protobuf::Message* message);
    virtual bool SendMsg(xmsg::XMsgHead* head, const google::protobuf::Message* message);
    virtual bool SendMsg(xmsg::XMsgHead* head, XMsg* msg);
    // 发送数据，添加标识
    virtual bool SendMsg(xmsg::XMsgHead* head, XMsg* msg, XMsgEvent* ev) { return false; }
    
    // 清理缓存消息头和消息内容，用于接收下一次消息
    void Clear();

    virtual void Close();

    using MsgCBFunc = void(XMsgEvent::*) (xmsg::XMsgHead* head, XMsg* msg);
    // 添加消息处理的回调函数，根据消息类型分发
    // type  消息类型
    // func  消息回调函数
    static void RegCB(xmsg::MsgType, MsgCBFunc func);

private:
    XMsg head_;
    XMsg msg_;

    // pb消息头
    xmsg::XMsgHead *pb_head_ = 0;
};

