#pragma once
#include <string>
#include <iostream>
#include <mutex>
#include <sstream>

#ifdef _WIN32
#ifdef XCOM_EXPORTS
#define XCOM_API __declspec(dllexport)
#else
#define XCOM_API __declspec(dllimport)
#endif
#else
#define XCOM_API
#endif

#define LOG(level, msg) std::cout << level << ":" << __FILE__ << ":" << __LINE__ << "\n" << msg << std::endl;
#define LOGDEBUG(msg) LOG("DEBUG", msg);
#define LOGINFO(msg) LOG("INFO", msg);
#define LOGERROR(msg) LOG("ERROR", msg);

XCOM_API std::string GetDirData(std::string path);

class XMutex
{
public:
    XMutex(std::mutex* mux)
    {
        mux_ = mux;
        mux_->lock();
    }
    ~XMutex()
    {
        mux_->unlock();
    }

    std::mutex* mux_ = 0;
};

