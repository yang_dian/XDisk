#include "xservice_handle.h"

void XServiceHandle::set_client_ip(const char* ip)
{
    if (!ip) return;
    strncpy(client_ip_, ip, sizeof(client_ip_));
}
