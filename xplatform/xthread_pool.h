#ifndef XTHREAD_POOL_H
#define XTHREAD_POOL_H
#include <vector>

#ifdef _WIN32
#ifdef XCOM_EXPORTS
#define XCOM_API __declspec(dllexport)
#else
#define XCOM_API __declspec(dllimport)
#endif
#else
#define XCOM_API
#endif

class XThread;
class XTask;
class XCOM_API XThreadPool
{
public:
    // 初始化所有线程并启动线程
    virtual void Init(int thread_count) = 0;

    // 分发线程
    virtual void Dispatch(XTask* task) = 0;

    // 退出所有的线程
    static void ExitAllThread();

    // 阻塞 等待ExitAllThread
    static void Wait();
};

class XCOM_API XThreadPoolFactory
{
public:
    static XThreadPool* Create();
};

#endif

