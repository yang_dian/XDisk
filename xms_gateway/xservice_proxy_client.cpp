#include "xservice_proxy_client.h"
#include "xtools.h"

using namespace std;

void XServiceProxyClient::ReadCB(xmsg::XMsgHead* head, XMsg* msg)
{
    if (!head || !msg) return;
    // 转发给XRouterHandle
    // 每个XServiceProxyClient对象可能管理多个XRouterHandle
    lock_guard<mutex> lock(callback_task_mutex_);
    auto router = callback_task_.find(head->msg_id());
    if (router == callback_task_.end())
    {
        LOGDEBUG("callback_task_ can't find.");
        return;
    }
    // 多线程问题？？
    router->second->SendMsg(head, msg);
}

bool XServiceProxyClient::SendMsg(xmsg::XMsgHead* head, XMsg* msg, XMsgEvent* ev)
{
    RegEvent(ev);
    head->set_msg_id((long long)ev);
    return XMsgEvent::SendMsg(head, msg);
}

void XServiceProxyClient::RegEvent(XMsgEvent* ev)
{
    lock_guard<mutex> lock(callback_task_mutex_);
    callback_task_[(long long)ev] = ev;
}

void XServiceProxyClient::DelEvent(XMsgEvent* ev)
{
    lock_guard<mutex> lock(callback_task_mutex_);
    callback_task_.erase((long long)ev);
}
