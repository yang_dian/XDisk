#ifndef XROUTER_HANDLE_H
#define XROUTER_HANDLE_H
#include "xservice_handle.h"
class XRouterHandle :
    public XServiceHandle
{
public:
    // 消息回调函数，默认发送到用户注册的函数，路由重载
    virtual void ReadCB(xmsg::XMsgHead* head, XMsg* msg);
    // 连接断开，超时，出错调用
    virtual void Close();
private:
    // 测试代码
    // char test_[100000000];
};

#endif

