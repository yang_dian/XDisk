#ifndef XSERVICE_PROXY_CLIENT_H
#define XSERVICE_PROXY_CLIENT_H
#include <map>
#include "xservice_client.h"
class XServiceProxyClient :
    public XServiceClient
{
public:
    // 消息回调函数，默认发送到用户注册的函数，路由重载
    virtual void ReadCB(xmsg::XMsgHead* head, XMsg* msg);
    // 发送数据，添加标识
    virtual bool SendMsg(xmsg::XMsgHead* head, XMsg* msg, XMsgEvent* ev);
    // 注册一个事件
    void RegEvent(XMsgEvent* ev);
    void DelEvent(XMsgEvent* ev);
private:
    // 消息转发的对象， 一个proxy对应多个XMsgEvent
    // 用指针的值作为索引，要兼容64位
    std::map<long long, XMsgEvent*> callback_task_;
    std::mutex callback_task_mutex_;
};

#endif

