#ifndef XROUTER_SERVER_H
#define XROUTER_SERVER_H

#include "xservice.h"
class XRouterServer :
    public XService
{
public:
    // 每个连接进入，调用此函数创建处理对象，加入到线程池
    virtual XServiceHandle* CreateServiceHandle();
};

#endif

