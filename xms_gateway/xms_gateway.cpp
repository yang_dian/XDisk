#include <iostream>
#include "xservice.h"
#include "xrouter_server.h"
#include "xservice_proxy.h"
using namespace std;

int main(int argc, char** argv)
{
    int server_port = API_GATEWAY_PORT;
    if (argc > 1)
        server_port = atoi(argv[1]);
    cout << "server_port is " << server_port << endl;
    // XDirServiceHandle::RegMsgCallback();
    XServiceProxy::Get()->Init();
    // 开启自动重连
    XServiceProxy::Get()->Start();
    XRouterServer service;
    service.set_server_port(server_port);
    service.Start();
    XThreadPool::Wait();
    return 0;
}