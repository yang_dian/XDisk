#include <thread>
#include "xservice_proxy.h"
#include "xmsg_com.pb.h"
#include "xtools.h"

using namespace std;
using namespace xmsg;

bool XServiceProxy::Init()
{
    // 1 从注册中心获取微服务列表
    // 测试数据
    XServiceMap service_map;
    auto smap = service_map.mutable_service_map();
    XServiceMap::XServiceList list;
    {
        auto service = list.add_service();
        service->set_ip("127.0.0.1");
        service->set_port(20011);
        service->set_name("dir");
    }
    {
        auto service = list.add_service();
        service->set_ip("127.0.0.1");
        service->set_port(20012);
        service->set_name("dir");
    }
    {
        auto service = list.add_service();
        service->set_ip("127.0.0.1");
        service->set_port(20013);
        service->set_name("dir");
    }
    
    (*smap)["dir"] = list;
    LOGDEBUG(service_map.DebugString());

    // 与微服务建立连接
    // 遍历XServiceMap数据
    for (auto m : (*smap))
    {
        client_map_[m.first] = vector<XServiceProxyClient*>();
        for (auto s : m.second.service())
        {
            auto proxy = new XServiceProxyClient();
            proxy->set_server_ip(s.ip().c_str());
            proxy->set_port(s.port());
            proxy->StartConnect();
            client_map_[m.first].push_back(proxy);
            client_map_last_index_[m.first] = 0;
        }
    }

    return true;
}

// 负载均衡找到客户端连接，进行数据发送
bool XServiceProxy::SendMsg(XMsgHead* head, XMsg* msg, XMsgEvent* ev)
{
    if (!head || !msg)
    {
        LOGDEBUG("XServiceProxy::SendMsg head or msg is null!");
        return false;
    }
    string service_name = head->service_name();
    // 1.负载均衡找到客户端连接
    auto client_list = client_map_.find(service_name);
    if (client_list == client_map_.end())
    {
        stringstream ss;
        ss << service_name << " client_map_ not fine!";
        LOGDEBUG(ss.str().c_str());
        return false;
    }
    // 轮询找到可用的微服务连接
    int cur_index = client_map_last_index_[service_name];
    int list_size = client_list->second.size();
    for (int i = 0; i < list_size; i++)
    {
        cur_index++;
        cur_index = cur_index % list_size;
        client_map_last_index_[service_name] = cur_index;
        auto client = client_list->second[cur_index];
        if (client->is_connected())
        {
            // 用于退出清理
            {
                lock_guard<mutex> lock(callbacks_mutex_);
                callbacks_[ev] = client;
            }
            // 转发消息
            return client->SendMsg(head, msg, ev);
        }
    }
    LOGDEBUG("can't find proxy");
    return false;
}

void XServiceProxy::DelEvent(XMsgEvent* ev)
{
    if (!ev)
    {
        return;
    }
    decltype(callbacks_.find(ev)) call;
    {
        lock_guard<mutex> lock(callbacks_mutex_);
        call = callbacks_.find(ev);
        if (call == callbacks_.end())
        {
            LOGDEBUG("callbacks_ not find!");
            return;
        }
    }
    call->second->DelEvent(ev);
}

void XServiceProxy::Start()
{
    thread th(&XServiceProxy::Main, this);
    th.detach();
}

void XServiceProxy::Stop()
{
}

void XServiceProxy::Main()
{
    while (!is_exit_)
    {
        // 从注册中心获取微服务的列表更新
        // 定时全部重新获取
        for (auto m : client_map_)
        {
            for (auto c : m.second)
            {
                if (c->is_connected())
                    continue;
                if (!c->is_connecting())
                {
                    LOGDEBUG("start connect service.");
                    c->Connect();
                }
            }
        }
        this_thread::sleep_for(3000ms);
    }
}
