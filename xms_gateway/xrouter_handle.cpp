#include "xrouter_handle.h"
#include "xtools.h"
#include "xservice_proxy.h"
using namespace std;

void XRouterHandle::ReadCB(xmsg::XMsgHead* head, XMsg* msg)
{
    // ת����Ϣ
    LOGDEBUG("XRouterHandle::ReadCB");
    XServiceProxy::Get()->SendMsg(head, msg, this);
}

void XRouterHandle::Close()
{
    XMsgEvent::Close();
    XServiceProxy::Get()->DelEvent(this);
}
