#include "xdiskgui.h"
#include <QtWidgets/QApplication>

#ifdef _WIN32
#include <windows.h>
#else
#include <signal.h>
#endif


int main(int argc, char *argv[])
{
#ifdef _WIN32
    // 初始化socket库
    WSADATA wver;
    WSAStartup(MAKEWORD(2, 2), &wver);
#else
    // 使用断开连接socket，会发出此信号，造成程序退出
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        return 1;
#endif

    QApplication a(argc, argv);
    XDiskGUI w;
    w.show();
    // 处理信号槽
    return a.exec();
}
