#include <string>
#include <QMessageBox>
#include "xdiskgui.h"
#include "xdisk_client.h"

using namespace std;

XDiskGUI::XDiskGUI(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
    XDiskClient::Get()->Init();
    // 注册信号支持的类型
    qRegisterMetaType<std::string>("std::string");
    // 绑定目录获取的信号
    QObject::connect(XDiskClient::Get(), SIGNAL(SDir(std::string)),this, SLOT(UpdateDir(std::string)));

    QObject::connect(XDiskClient::Get(), SIGNAL(SUploadComplete()), this, SLOT(Refresh()));

    QObject::connect(XDiskClient::Get(), SIGNAL(SDownloadComplete()), this, SLOT(DownloadComplete()));

    Refresh();
}

void XDiskGUI::UpdateSeverInfo()
{
    // 服务器路径 服务器IP 服务器端口
    string ip = ui.ipEdit->text().toStdString();
    string root = ui.pathEdit->text().toStdString();
    int port = ui.portBox->value();
    // QMessageBox::information(this, "", "Refresh");
    XDiskClient::Get()->set_port(port);
    XDiskClient::Get()->set_server_ip(ip);
    XDiskClient::Get()->set_server_root(root);
}


void XDiskGUI::Refresh()
{
    UpdateSeverInfo();
    XDiskClient::Get()->GetDir();

    // 1 连接服务器

    // 2 设置回调
}


void XDiskGUI::UpdateDir(std::string dirs)
{
    // QMessageBox::information(this, "", dirs.c_str());
    // "file1,1024;file2,4096;file3.zip,10240"
    QString str = dirs.c_str();
    str = str.trimmed();
    if (str.isEmpty())
    {
        return;
    }
    QStringList filestr = str.split(';');
    ui.filelistWidget->setRowCount(filestr.size());
    for (int i = 0; i < filestr.size(); i++)
    {
        QStringList fileinfo = filestr[i].split(',');
        if (fileinfo.size() != 2)
        {
            continue;
        } 
        // 插入到列表
        ui.filelistWidget->setItem(i, 0, new QTableWidgetItem(fileinfo[0]));
        ui.filelistWidget->setItem(i, 1, new QTableWidgetItem(tr("%1Byte").arg(fileinfo[1])));
    }
}

void XDiskGUI::Upload()
{
    
    // 用户选择一个文件
    QString filename = QFileDialog::getOpenFileName(this, QString::fromLocal8Bit("请选择上传文件"));
    if (filename.isEmpty())
        return;
    UpdateSeverInfo();
    XDiskClient::Get()->Upload(filename.toStdString());
    
    // 插入到列表
    //ui.filelistWidget->insertRow(0); // 插入在开头位置
    //ui.filelistWidget->setItem(0, 0, new QTableWidgetItem(filename));
    //ui.filelistWidget->setItem(0, 1, new QTableWidgetItem(tr("%1Byte").arg(1900)));
}

void XDiskGUI::Download()
{
    // 用户选择下载的文件和路径
    UpdateSeverInfo();
    int row = ui.filelistWidget->currentRow();
    if (row < 0)
    {
        QMessageBox::information(this, "", QString::fromLocal8Bit("请选择下载文件"));
        return;
    }
    // 获取选择的文件名
    auto* item = ui.filelistWidget->item(row, 0);
    string filename = item->text().toStdString();
    // 获取下载路径
    QString localpath = QFileDialog::getExistingDirectory(this, QString::fromLocal8Bit("请选择下载路径"));
    if (localpath.isEmpty())
    {
        return;
    }
    string filepath = ui.pathEdit->text().toStdString();
    filepath += "/";
    filepath += filename;
    XDiskClient::Get()->Download(filepath, localpath.toStdString());
}

void XDiskGUI::DownloadComplete()
{
    QMessageBox::information(this, "", "download complete");
}
