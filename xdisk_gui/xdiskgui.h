#pragma once

#include <QtWidgets/QWidget>
#include <QFileDialog>
#include "ui_xdiskgui.h"

class XDiskGUI : public QWidget
{
    Q_OBJECT

public:
    XDiskGUI(QWidget *parent = Q_NULLPTR);

    void UpdateSeverInfo();
public slots:
    void Refresh();
    void UpdateDir(std::string dirs);
    void Upload();
    void Download();
    void DownloadComplete();
private:
    Ui::XDiskGUIClass ui;
};
