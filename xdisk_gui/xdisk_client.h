#pragma once
#include <string>
#include <QObject>
class XDiskClient : public QObject
{
    Q_OBJECT 
public:
    // 单件
    static XDiskClient* Get()
    {
        static XDiskClient xc;
        return &xc;
    }

    // 初始化，包括线程池
    bool Init();
    
    // 获取目录
    void GetDir();

    // 上传文件请求
    void Upload(std::string filepath);

    // 上传文件请求
    void Download(std::string serverpath, std::string localdir);

    void set_server_ip(std::string ip) { server_ip_ = ip; }
    void set_port(int port) { server_port_ = port; }
    void set_server_root(std::string root) { server_root_ = root; }

signals:
    void SDir(std::string dirs);
    void SUploadComplete();
    void SDownloadComplete();
private:
    // 服务器IP
    std::string server_ip_ = "";

    // 服务器目录
    std::string server_root_ = "";

    // 服务器端口
    int server_port_ = 0;
    XDiskClient();
};

