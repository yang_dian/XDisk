#include <iostream>
#include <event2/event.h>

using namespace std;

int main()
{
#ifdef _WIN32
    // 初始化socket库
    WSADATA wver;
    WSAStartup(MAKEWORD(2,2),&wver);
#endif

    cout << "test libevent!" << endl;
    // 创建libevent的上下文
    event_base * base = event_base_new();
    if (base)
    {
        cout << "event_base_new success!" << endl;
    }
    return 0;
}