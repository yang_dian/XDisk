#include <iostream>
#include <thread>
#include <mysql.h>
#include <string>
#include <sstream>
using namespace std;

int main()
{
    mysql_library_init(0, 0, 0);
    MYSQL mysql;
    mysql_init(&mysql);
    const char* host = "127.0.0.1";
    const char* user = "root";
    const char* pass = "123456";
    const char* db = "test"; // 数据库名称

    // 设定超时3秒
    int to = 3;
    int re = mysql_options(&mysql, MYSQL_OPT_CONNECT_TIMEOUT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 自动重连
    int recon = 1;
    re = mysql_options(&mysql, MYSQL_OPT_RECONNECT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 连接登录数据库
    if (!mysql_real_connect(&mysql, host, user, pass, db, 3306, 0, 0))
    {
        cout << "mysql_real_connect failed! " << mysql_error(&mysql) << endl;
    }
    else {
        cout << "mysql_real_connect success! " << host << endl;
    }

    // 1 创建表
    string sql = "";
    sql = "create table if not exists `t_image` (`id` int auto_increment, `name` varchar(1024), `path` varchar(2046), `size` int, primary key (`id`))";
    re = mysql_query(&mysql, sql.c_str());
    if (re != 0)
    {
        cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
    }
    //2 插入数据
    for (int i = 0; i < 1000; i++)
    {
        stringstream ss;
        ss << "insert into `t_image` (`name`, `path`, `size`) values('image";
        ss << i << ".jpg'" << R"(, 'd:\\img\\', 10240))";
        sql = ss.str();
        re = mysql_query(&mysql, sql.c_str());
        if (re == 0)
        {
            int count = mysql_affected_rows(&mysql);
            cout << "mysql_affected_rows " << count << " id = " << mysql_insert_id(&mysql) <<  endl;
        }
        else
        {
            cout << "insert failed!" << mysql_error(&mysql) << endl;
        }
    }
    
    mysql_close(&mysql);
    mysql_library_end();
    return 0;
}



//// 单线程模式 mysql_init自动调用 ，线程不安全
//mysql_library_init(0, 0, 0);
//for (;;)
//{
//    MYSQL* mysql = new MYSQL();
//    mysql_init(mysql);
//    mysql_close(mysql);
//    delete mysql;
//}
//
//mysql_library_end();