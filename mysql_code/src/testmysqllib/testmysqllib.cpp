#include <iostream>
#include <thread>
#include <mysql.h>

using namespace std;

int main()
{
    MYSQL mysql;
    mysql_init(&mysql);
    const char* host = "127.0.0.1";
    const char* user = "root";
    const char* pass = "123456";
    const char* db = "mysql"; // 数据库名称

    // 设定超时3秒
    int to = 3;
    int re = mysql_options(&mysql, MYSQL_OPT_CONNECT_TIMEOUT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 自动重连
    int recon = 1;
    re = mysql_options(&mysql, MYSQL_OPT_RECONNECT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 连接登录数据库
    if (!mysql_real_connect(&mysql, host, user, pass, db, 3306, 0, 0))
    {
        cout << "mysql_real_connect failed! " << mysql_error(&mysql) << endl;
    }
    else {
        cout << "mysql_real_connect success! " << host << endl;
    }

    for (;;)
    {
        int re = mysql_ping(&mysql);
        if (re == 0)
        {
            cout << host << " mysql ping success!" << endl;
        }
        else {
            cout << host << " mysql ping failed! " << mysql_error(&mysql) << endl;
        }
        this_thread::sleep_for(1s);
    }
    
    
    return 0;
}



//// 单线程模式 mysql_init自动调用 ，线程不安全
//mysql_library_init(0, 0, 0);
//for (;;)
//{
//    MYSQL* mysql = new MYSQL();
//    mysql_init(mysql);
//    mysql_close(mysql);
//    delete mysql;
//}
//
//mysql_library_end();