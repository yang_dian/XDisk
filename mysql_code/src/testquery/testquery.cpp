#include <iostream>
#include <thread>
#include <mysql.h>

using namespace std;

int main()
{
    mysql_library_init(0, 0, 0);
    MYSQL mysql;
    mysql_init(&mysql);
    const char* host = "127.0.0.1";
    const char* user = "root";
    const char* pass = "123456";
    const char* db = "mysql"; // 数据库名称

    // 设定超时3秒
    int to = 3;
    int re = mysql_options(&mysql, MYSQL_OPT_CONNECT_TIMEOUT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 自动重连
    int recon = 1;
    re = mysql_options(&mysql, MYSQL_OPT_RECONNECT, &to);
    if (re != 0)
    {
        cout << "mysql_options failed! " << mysql_error(&mysql) << endl;
    }

    // 连接登录数据库
    if (!mysql_real_connect(&mysql, host, user, pass, db, 3306, 0, 0))
    {
        cout << "mysql_real_connect failed! " << mysql_error(&mysql) << endl;
    }
    else {
        cout << "mysql_real_connect success! " << host << endl;
    }

    // user select * from user
    // 1 执行SQL语句
    const char* sql = "select * from user";
    // mysql_real_query sql语句中可以包含二进制数据
    // mysql_query  sql语句中只能是字符串
    // 执行sql语句后，必需获取结果集，并且清理
    re = mysql_real_query(&mysql, sql, strlen(sql));
    if (re != 0)
    {
        cout << "mysql_real_query failed! " << mysql_error(&mysql) << endl;
    }
    else {
        cout << "mysql_real_query success! " << sql << endl;
    }
    //2 获取结果集
    // 不实际读取
    // MYSQL_RES* result = mysql_use_result(&mysql);
    // 读取所有数据，注意缓存大小
    MYSQL_RES* result = mysql_store_result(&mysql);
    if (!result)
    {
        cout << "mysql_use_result failed! " << mysql_error(&mysql) << endl;
    }
    //3 遍历结果集
    MYSQL_ROW row;
    while (row = mysql_fetch_row(result))
    {
        auto lens = mysql_fetch_lengths(result);
        cout << lens[0] << "[" << row[0] << "," << row[1] << "]" << endl;
    }

    // 清理结果集
    mysql_free_result(result);
    
    mysql_close(&mysql);
    mysql_library_end();
    return 0;
}



//// 单线程模式 mysql_init自动调用 ，线程不安全
//mysql_library_init(0, 0, 0);
//for (;;)
//{
//    MYSQL* mysql = new MYSQL();
//    mysql_init(mysql);
//    mysql_close(mysql);
//    delete mysql;
//}
//
//mysql_library_end();