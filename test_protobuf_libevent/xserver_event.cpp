#include <iostream>
#include <sstream>
#include "xmsg_com.pb.h"
#include "xserver_event.h"

using namespace std;
using namespace xmsg;
std::map<xmsg::MsgType, XServerEvent::MsgCBFunc> XServerEvent::calls_;

void XServerEvent::Init()
{
    RegCB(MSG_LOGIN_REQ, &XServerEvent::LoginReq);
}

void XServerEvent::LoginReq(const char* data, int size)
{
    cout << "LoginReq " << size << endl;
    // 反序列化
    XLoginReq req;
    req.ParseFromArray(data, size);
    cout << req.DebugString() << endl;

    // 返回消息
    XLoginRes res;
    res.set_res(XLoginRes::OK);
    string token = req.username();
    token += "sign";
    res.set_token(token);
    SendMsg(MSG_LOGIN_RES, &res);
}
