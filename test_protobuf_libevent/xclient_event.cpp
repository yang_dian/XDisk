#include <iostream>
#include <sstream>
#include "xmsg_com.pb.h"
#include "xclient_event.h"
using namespace std;
using namespace xmsg;
std::map<xmsg::MsgType, XClientEvent::MsgCBFunc> XClientEvent::calls_;

void XClientEvent::Init()
{
    RegCB(MSG_LOGIN_RES, &XClientEvent::LoginRes);
}

void XClientEvent::LoginRes(const char* data, int size)
{
    cout << "LoginRes " << size << endl;
    // ��� �����л�
    XLoginRes res;
    res.ParseFromArray(data, size);
    cout << res.DebugString() << endl;

    static int count = 0;
    ostringstream os;
    os << "root_" << count++;
    xmsg::XLoginReq req;
    req.set_username(os.str());
    req.set_password("123456");
    SendMsg(MSG_LOGIN_REQ, &req);
}
