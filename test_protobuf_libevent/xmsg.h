#pragma once
#include "xmsg_type.pb.h"
// 头部消息的最大字节数
#define MAX_MSG_SIZE 8192


// 所有的函数做内联
class XMsg
{
public:
    // 数据大小
    int size_ = 0;
    // 消息类型
    xmsg::MsgType type_ = xmsg::NONE_DO_NOT_USE;
    // 数据存放(protobuf的序列化后的数据）
    char* data_ = 0;
    // 已经接收的数据大小
    int recv_size_ = 0;
    
    bool Alloc(int s)
    {
        if (s <= 0 || s > MAX_MSG_SIZE)
            return false;
        if (data_)
            delete data_;
        data_ = new char[s];
        if (!data_)
            return false;
        this->size_ = s;
        this->recv_size_ = 0;
        return true;
    }

    // 判断数据是否接收完成
    bool Recved()
    {
        if (size_ <= 0) return false;
        return (recv_size_ == size_);
    }

    void Clear()
    {
        delete data_;
        memset(this, 0, sizeof(*this));
    }
};

