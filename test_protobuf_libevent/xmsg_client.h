#pragma once
#include <string>
class XMsgClient
{
public:
    // 启动测试线程，发送数据给服务端（延时启动）
    void Start();

    void set_server_port(int port) { server_port_ = port; }
    void set_server_ip(std::string ip) { server_ip_ = ip; }

    // 线程主函数
    void Main();
private:

    int server_port_;
    std::string server_ip_;
};

