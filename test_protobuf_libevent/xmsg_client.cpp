#include <iostream>
#include <thread>
#include <chrono>
#include <sstream>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include "xmsg_com.pb.h"
#include "xmsg_type.pb.h"
#include "xmsg_client.h"
#include "xclient_event.h"

using namespace std;
using namespace chrono;
using namespace xmsg;


static void ReadCB(struct bufferevent* bev, void* ctx)
{
    cout << "#" << endl;
    auto ev = (XClientEvent*)ctx;

    if (!ev->RecvMsg())
    {
        ev->Clear();
        delete ev;
        bufferevent_free(bev);
        return;
    }
    auto* msg = ev->GetMsg();
    if (!msg)
    {
        return;
    }
    ev->CallFun(msg->type_, msg->data_, msg->size_);
    ev->Clear();

    this_thread::sleep_for(milliseconds(500));
}

static void EventCB(struct bufferevent* bev, short what, void* ctx)
{
    cout << "client event" << endl;
    auto ev = (XClientEvent*)ctx;
    // 读超时
    if (what & BEV_EVENT_TIMEOUT || what & BEV_EVENT_ERROR || what & BEV_EVENT_EOF)
    {
        cout << "BEV_EVENT_TIMEOUT || BEV_EVENT_ERROR || BEV_EVENT_EOF" << endl;
        // 读取缓冲中内容
        ev->Clear();
        delete ev;
        // 清理空间，关闭监听
        bufferevent_free(bev);
        return;
    }
    if (what & BEV_EVENT_CONNECTED)
    {
        cout << "BEV_EVENT_CONNECTED" << endl;
        // bufferevent_write(bev, "OK", 3);
        xmsg::XLoginReq req;
        req.set_username("root");
        req.set_password("123456");
        ev->SendMsg(MSG_LOGIN_REQ, &req);
    }
}

// 启动测试线程，发送数据给服务端（延时启动）
void XMsgClient::Start()
{
    thread th(&XMsgClient::Main, this);
    th.detach();
}

// 线程主函数
void XMsgClient::Main()
{
    // 注册消息回调函数
    XClientEvent::Init();

    if (server_port_ <= 0)
    {
        cerr << "client error: please set the server port" << endl;
    }

    // 等服务端启动
    this_thread::sleep_for(milliseconds(200));
    cout << "XMsgClient::Main begin" << endl;
    event_base* base = event_base_new();
    // 连接服务端
    bufferevent* bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
    sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(server_port_);
    if (server_ip_.empty())
    {
        server_ip_ = "127.0.0.1";
    }
    evutil_inet_pton(AF_INET, server_ip_.c_str(), &sin.sin_addr.s_addr);

    // 设置回调函数
    // 添加监控事件 设置内部权限参数
    bufferevent_enable(bev, EV_READ | EV_WRITE);

    // 超时设定 秒，微秒（1/1000000秒） 读超时和写超时
    timeval t1 = { 30, 0 };
    bufferevent_set_timeouts(bev, &t1, 0);

    auto* ev = new XClientEvent();
    ev->set_bev(bev);

    // 设置回调函数
    bufferevent_setcb(bev, ReadCB, 0, EventCB, ev);

    int re = bufferevent_socket_connect(bev, (sockaddr*)&sin, sizeof(sin));
    if (re != 0)
    {
        cerr << "bufferevent_socket_connect error" << endl;
        return;
    }

    // 事件主循环，监控事件是否发生，分发事件到回调函数
   // 如果没有事件注册则退出
    event_base_dispatch(base);
    event_base_free(base);

    cout << "XMsgClient::Main end" << endl;
}
