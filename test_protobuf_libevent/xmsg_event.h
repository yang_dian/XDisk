#pragma once
#include "xmsg_type.pb.h"
#include "xmsg.h"
class XMsgEvent
{
public:
    // 接收数据包，1 正确接收到消息 （调用消息处理函数）  2 消息接收不完整，（等待下一次接收） 3 消息接收出错（退出清理空间）
    // return: 1 2 返回true  3 返回false
    bool RecvMsg();
    class XMsg* GetMsg();

    // 发送消息，包含头部（自动创建）
    void SendMsg(xmsg::MsgType type, const google::protobuf::Message* message);
    
    // 清理缓存消息头和消息内容，用于接收下一次消息
    void Clear();

    void set_bev(struct bufferevent* bev) { bev_ = bev; }
private:
    XMsg head_;
    XMsg msg_;
    struct bufferevent* bev_ = 0;
};

