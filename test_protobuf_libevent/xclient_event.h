#pragma once
#include <map>
#include "xmsg_event.h"
#include "xmsg_type.pb.h"
class XClientEvent :
    public XMsgEvent
{
public:
    // 初始化回调函数
    static void Init();
    // 接收登录反馈消息
    void LoginRes(const char* data, int size);

    // 成员函数指针类型
    typedef void (XClientEvent::* MsgCBFunc)(const char* data, int size);
    // 注册消息回调函数，只需要注册一次，存在静态map
    // type:消息类型
    // func:消息回调函数
    static void RegCB(xmsg::MsgType type, MsgCBFunc func)
    {
        calls_[type] = func;
    }

    void CallFun(xmsg::MsgType type, const char* data, int size)
    {
        if (calls_.find(type) != calls_.end())
        {
            // 通过成员函数指针调用
            (this->*(calls_[type]))(data, size);
        }
    }

private:
    // 存放回调函数
    static std::map<xmsg::MsgType, MsgCBFunc> calls_;
};

