#include <iostream>
#include <event2/bufferevent.h>
#include "xmsg_event.h"
#include "xmsg_com.pb.h"

using namespace std;
using namespace xmsg;
using namespace google;
using namespace protobuf;

bool XMsgEvent::RecvMsg()
{
    if (!bev_)
    {
        cerr << "XMsgEvent::RecvMsg() failed: bev not set" << endl;
        return false;
    }
    // 解包
    // 1 消息头大小
    if (!head_.size_)
    {
        // 消息头大小
        int len = bufferevent_read(bev_, &head_.size_, sizeof(head_.size_));
        if (len <= 0 || head_.size_ <= 0)
        {
            return false;
        }
        // 分配消息头空间 读取消息头（鉴权，消息大小）
        if (!head_.Alloc(head_.size_))
        {
            cerr << "head_.Alloc failed!" << endl;
            return false;
        }
    }
    
    // 2 开始接收消息头 （鉴权，消息大小）
    if (!head_.Recved())
    {
        int len = bufferevent_read(bev_, head_.data_ + head_.recv_size_, head_.size_ - head_.recv_size_);
        if (len <= 0)
        {
            return true;
        }
        head_.recv_size_ += len;
        if (!head_.Recved())
        {
            return true;
        }
        // 完整的头部数据接收完成
        // 反序列化
        XMsgHead pb_head;
        if (!pb_head.ParseFromArray(head_.data_, head_.size_))
        {
            cerr << "pb_head.ParseFromArray failed!" << endl;
            return false;
        }
        // 鉴权
        // 消息内容大小
        // 分配消息内容空间
        if (!msg_.Alloc(pb_head.msg_size()))
        {
            cerr << "msg_.Alloc failed!" << endl;
            return false;
        }
        // 消息类型
        msg_.type_ = pb_head.msg_type();
    }

    // 3 开始接收消息内容
    if (!msg_.Recved())
    {
        int len = bufferevent_read(bev_, msg_.data_ + msg_.recv_size_, msg_.size_ - msg_.recv_size_);
        if (len <= 0)
        {
            return true;
        }
        msg_.recv_size_ += len;
    }
    if (msg_.Recved())
    {
        cout << "msg_.Recved()" << endl;
    }

    return true;
}

XMsg* XMsgEvent::GetMsg()
{
    if (msg_.Recved())
        return &msg_;
    return nullptr;
}

void XMsgEvent::SendMsg(xmsg::MsgType type, const Message* message)
{
    if (!bev_ || !message)
    {
        return;
    }
    XMsgHead head;
    head.set_msg_type(type);
    // 封包
    // 消息内容序列化
    string msg_str = message->SerializeAsString();
    int msg_size = msg_str.size();
    head.set_msg_size(msg_size);
    // 消息头序列化
    string head_str;
    cout << head.DebugString() << endl;
    head.SerializeToString(&head_str);
    int headsize = head_str.size();
    
    // 1 发送消息头大小 4字节 暂时不考虑字节序问题
    bufferevent_write(bev_, &headsize, sizeof(headsize));
    // 2 发送消息头（pb序列化）XMsgHead (设置消息内容的大小）
    bufferevent_write(bev_, head_str.data(), headsize);
    // 3 发送消息内容 （pb序列化）业务proto
    bufferevent_write(bev_, msg_str.data(), msg_size);
}

void XMsgEvent::Clear()
{
    head_.Clear();
    msg_.Clear();
}
