#include <string.h>
#include <stdlib.h>
#include <iostream>
#include "xmsg_server.h"
#include "xmsg_client.h"
#ifdef _WIN32
// 和protobuf头文件会有冲突，protobuf的头文件要在windows.h之前
#include <windows.h>
#else
#include <signal.h>
#endif

using namespace std;

int main(int argc, char *argv[])
{
    int server_port = 20010;
    if (argc > 1)
    {
        server_port = atoi(argv[1]);
    }
#ifdef _WIN32
    // 初始化socket库
    WSADATA wver;
    WSAStartup(MAKEWORD(2,2),&wver);
#else
    // 使用断开连接socket，会发出此信号，造成程序退出
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        return 1;
#endif
    XMsgClient client;
    client.set_server_port(server_port);
    client.Start();
    

    XMsgServer server;
    server.Init(server_port);

    return 0;
}