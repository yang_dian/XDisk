#include <iostream>
#include "xservice.h"
#include "xdir_service_handle.h"
using namespace std;
class XTestService : public XService
{
public:
    // 每个连接进入，调用此函数创建处理对象，加入到线程池
    virtual XServiceHandle* CreateServiceHandle()
    {
        return new XDirServiceHandle();
    }
};

int main(int argc, char** argv)
{
    int server_port = 20011;
    if (argc > 1)
        server_port = atoi(argv[1]);
    cout << "server_port is " << server_port << endl;
    XDirServiceHandle::RegMsgCallback();
    XTestService service;
    service.set_server_port(server_port);
    service.Start();
    XThreadPool::Wait();
    return 0;
}