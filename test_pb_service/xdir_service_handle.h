#ifndef XDIR_SERVICE_HANDLE_H
#define XDIR_SERVICE_HANDLE_H
#include "xservice_handle.h"
class XDirServiceHandle :
    public XServiceHandle
{
public:
    XDirServiceHandle();
    ~XDirServiceHandle();
    // 处理用户的目录请求
    void DirReq(xmsg::XMsgHead* head, XMsg* msg);

    // 注册消息回调函数
    static void RegMsgCallback();
};

#endif

