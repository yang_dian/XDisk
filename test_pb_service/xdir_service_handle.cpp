#include "xmsg_com.pb.h"
#include "xdir_service_handle.h"
#include "xtools.h"

using namespace std;
using namespace xmsg;

void XDirServiceHandle::RegMsgCallback()
{
    RegCB(MSG_DIR_REQ, (MsgCBFunc)&XDirServiceHandle::DirReq);
}

XDirServiceHandle::XDirServiceHandle()
{
}

XDirServiceHandle::~XDirServiceHandle()
{
}

void XDirServiceHandle::DirReq(xmsg::XMsgHead* head, XMsg* msg)
{
    if (!head || !msg)
    {
        LOGDEBUG("XDirServiceHandle::DirReq msg error!");
        return;
    }
    XDirReq req;
    if (!req.ParseFromArray(msg->data_, msg->size_))
    {
        LOGDEBUG("req.ParseFromArray failed!");
        return;
    }
    LOGDEBUG(req.DebugString());
    // 响应客户端 头部信息保留，用于路由
    XDirRes res;
    // 测试代码
    res.set_res(XDirRes::OK);
    stringstream ss;
    for (int i = 0; i < 10; i++)
    {
        static int count = 0;
        count++;
        stringstream ss;
        ss << "filename_" << count << "_" << i;
        auto dir = res.add_dirs();
        dir->set_filename(ss.str());
        dir->set_filesize((i + 1) * 1024);
    }
    head->set_msg_type(MSG_DIR_RES);
    SendMsg(head, &res);
}
