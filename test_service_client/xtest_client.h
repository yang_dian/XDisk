#pragma once
#include "xservice_client.h"
#include "xmsg_com.pb.h"

class XTestClient :
    public XServiceClient
{
public:
    virtual void ConnectedCB();

    // path：请求的根目录
    // return 是否请求成功，不保证目录获取
    bool GetDir(std::string path);

    // GetDir 目录请求响应
    void DirRes(xmsg::XMsgHead* head, XMsg* msg);

    // 检查连接，自动重连，连接失败立刻返回， 已连接立刻返回
    // return 连接成功返回 true  
    bool AutoConnect(int timeout_ms);

    static void RegMsgCallback()
    {
        RegCB(xmsg::MSG_DIR_RES, (MsgCBFunc)&XTestClient::DirRes);
    }
};

