#include <iostream>
#include <sstream>
#include <thread>
#include "xtest_client.h"

using namespace std;
int main(int argc, char** argv)
{
    XTestClient::RegMsgCallback();
    XTestClient* client = new XTestClient();
    client->set_server_ip("127.0.0.1");
    client->set_port(API_GATEWAY_PORT);
    client->StartConnect();
    for (int i = 0; i < 100000; i++)
    {
        stringstream ss;
        ss << "/root/" << i;
        client->GetDir(ss.str());
        this_thread::sleep_for(1000ms);
    }
    XThreadPool::Wait();
    return 0;
}