#include <thread>
#include <sstream>
#include "xtest_client.h"
#include "xtools.h"
#include "xmsg_com.pb.h"

using namespace std;
using namespace xmsg;

void XTestClient::ConnectedCB()
{
    LOGDEBUG("ConnectedCB");

}

bool XTestClient::GetDir(std::string path)
{
    if (!AutoConnect(300))
    {
        return false;
    }
    stringstream ss;
    ss << "XTestClient::GetDir " << path;
    LOGDEBUG(ss.str().c_str());
    // 发送pb的消息给服务
    XDirReq req;
    req.set_path(path);
    XMsgHead head;
    head.set_msg_type(MSG_DIR_REQ);
    head.set_service_name("dir");
    head.set_token("test token");
    return SendMsg(&head, &req);
}

void XTestClient::DirRes(xmsg::XMsgHead* head, XMsg* msg)
{
    if (!head || !msg)
    {
        LOGDEBUG("XTestClient::DirRes msg error!");
        return;
    }
    XDirRes res;
    if (!res.ParseFromArray(msg->data_, msg->size_))
    {
        LOGDEBUG("res.ParseFromArray error!");
        return;
    }
    LOGDEBUG(head->DebugString());
    LOGDEBUG(res.DebugString());
}

bool XTestClient::AutoConnect(int timeout_ms)
{
    // 1 已连接
    if (is_connected())
        return true;
    // 2 未连接， 也不在连接中
    if (!is_connecting())
    {
        // 开始连接
        if (!Connect())
        {
            return false;
        }
    }

    int count = timeout_ms / 10;
    // 连接中
    for (int i = 0; i < count; i++)
    {
        if (is_connected())
            return true;
        if (!is_connecting())
            break;
        this_thread::sleep_for(10ms);
    }
    if (is_connected())
        return true;
    return false;
}
